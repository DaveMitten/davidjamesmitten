import React from 'react';
import ReactDOM from 'react-dom';
import 'babel-polyfill';
import 'stylus/app.styl';
import App from './app';

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
